��          |       �       �   x   �      V  �   _       2   &     Y     w     �  U   �     �  H   �  �   F  �   =  &   �  �    d   �  �      �   �  `   Q     �  �   �  ?   �	  �   �	   <a href="http://codex.wordpress.org/Network_Admin_Settings_Screen" target="_blank">Documentation on Network Settings</a> Add User Add the designated lines of code to wp-config.php (just before <code>/*...stop editing...*/</code>) and <code>.htaccess</code> (replacing the existing WordPress rules). Enter the username and email. New site created by %1$s

Address: %2$s
Name: %3$s Select a user to change role. Select a user to remove. Site: %s Subdirectory networks may not be fully compatible with custom wp-content directories. User created. Warning! User cannot be deleted. The user %s is a network administrator. PO-Revision-Date: 2014-03-28 03:46:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Network Admin
 <a href="http://codex.wordpress.org/Network_Admin_Settings_Screen" target="_blank">வலையமைப்பு அமைவுகளின் ஆவணமாக்கம்</a>  பயனர் சேர்க்க  குறிக்கப்பட்டுள்ள நிரலை wp-config.php கோப்பில் (<code>/*...stop editing...*/</code> க்கு முன்னும் ) மற்றும் <code>.htaccess</code> ( முன்பு இருக்கும் வோர்ட்பிரஸ் விதிகளை மீள்மாற்றம் செய்தும்) சேர்க்க.  பயனர்பெயரும் மின்னஞ்சலும் உள்ளிடுக.  புதிய தளம்  %1$s
என்பவரால் உருவாக்கப்பட்டுள்ளது

முகவரி: %2$s
பெயர்: %3$s  தகுதிமாற்றம் செய்யவேண்டிய பயனரை குறிக்கக்வும்.  நீக்க வேண்டியப் பயனரை குறிக்கவும்.   தளம்: %s  உள்கோப்புறை வலையமைப்புகள் தன்விருப்ப wp-content கோப்புறைகளுடன் முழு இணக்கமற்றதாய் இருக்ககூடும்.  பயனர் இணைக்கப்பட்டார்.  எச்சரிக்கை! பயனரை நீக்க முடியாது.பயனர் %s ஒர் வலையமைப்பு மேலாளராவார்.  